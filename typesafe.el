(require 'cl-lib)
(require 'slime)

(defun read-from-file (start end)
  "Read compiled forms from file associated with current buffer.
   See `slime-compile-file'."
  (let ((file (slime-to-lisp-filename (buffer-file-name))))
    ;; TODO: read-from-file
    nil))

(defun read-from-buffer (start end)
  "Read compiled forms from current buffer.
   See `slime-compile-region'."
  (let ((content (buffer-substring-no-properties start end)))
    (read-from-string content nil nil)))

(defvar *last-make-instance-types* nil
  "List of symbols used in `make-instance'. Are collected before compilation,
   are checks for being types after compilation.")

(defun traverse (form)
  "Drop old types used in `make-instance', collect new ones"
  (setf *last-make-instance-types* nil)
  (traverse-aux form))

(defun traverse-aux (form)
  "Collect symbols used in `make-instance' into `*last-make-instance-types*'"
  (when (listp form)
    ;; if current form is `make-instance' - save used type for later
    (when (eq (car form) 'cl:make-instance)
      (push (second form) *last-make-instance-types*))
    ;; then traverse inner forms
    (mapc #'traverse-aux form)))

(defun typesafe-before-compile (start end)
  "Entry point. Is called before each compilation, collects all symbols,
   used in `make-instance' from compiled region."
  (traverse (read-from-buffer start end)))

(eval-when-compile
  (push #'typesafe-before-compile slime-before-compile-functions))

(defun check-instance-types ()
  (mapc #'check-instance-type *last-make-instance-types*))

(defun check-instance-type (cons-type)
  (let ((type (when (and (consp cons-type)
                         (eq (car cons-type) 'cl:quote))
                ;; TODO: else - handle symbols somehow
                (second cons-type))))
    (when type
      (typep t type))))

(defvar *old-slime-hook* slime-compilation-finished-hook
  "Saved original hook for finished compilation. 
   Is called with `typesafe-compilation-finished'.")

(defun typesafe-compilation-finished (notes)
  "Entry point. Is called after each compilation, checks all collected symbols,
   used in `make-instance' for being accessible types.
   See `slime-compilation-finished-hook'."
  ;; TODO: maybe reverse call order?
  (check-instance-types)
  (funcall *old-slime-hook* notes))

(eval-when-compile
  ;; (setf slime-compilation-finished-hook #'typesafe-compilation-finished)
  )

(provide 'typesafe)
